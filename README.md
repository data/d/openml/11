# OpenML dataset: balance-scale

https://www.openml.org/d/11

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Siegler, R. S. (donated by Tim Hume)  
**Source**: [UCI](http://archive.ics.uci.edu/ml/datasets/balance+scale) - 1994  
**Please cite**: [UCI](https://archive.ics.uci.edu/ml/citation_policy.html)   

**Balance Scale Weight & Distance Database**  
This data set was generated to model psychological experimental results.  Each example is classified as having the balance scale tip to the right, tip to the left, or be balanced. The attributes are the left weight, the left distance, the right weight, and the right distance. The correct way to find the class is the greater of (left-distance * left-weight) and (right-distance * right-weight). If they are equal, it is balanced.

### Attribute description  
The attributes are the left weight, the left distance, the right weight, and the right distance.

### Relevant papers  
Shultz, T., Mareschal, D., & Schmidt, W. (1994). Modeling Cognitive Development on Balance Scale Phenomena. Machine Learning, Vol. 16, pp. 59-88.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/11) of an [OpenML dataset](https://www.openml.org/d/11). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/11/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/11/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/11/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

